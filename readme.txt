This solution run in Python 2.7 and Django 1.11.3

Dependencies are:
- DJango 1.11.3
- Numpy 1.13.1
- djangorestframework	3.6.3

Enviroment with virtualenv 15.5

Install

1. Install Python
2. Install virtualenv https://virtualenv.pypa.io/en/stable/installation/
3. Active Virtualenv https://virtualenv.pypa.io/en/stable/userguide/
4. Clone repository GIT in https://gitlab.com/andresr56/matrix-uni/
4. In the active virtual environment install django https://docs.djangoproject.com/en/1.11/intro/install/
5. In the active virtual Install djangorestframework http://www.django-rest-framework.org/
6. In the active virtual Install Numpy https://docs.scipy.org/doc/numpy-1.10.1/user/install.html
7. In the project's main folder and in the active virtual run server with the next command 'python manage.py runserver' 
8. If everything goes fine. Open an internet browser and enter the given ip, example: http://127.0.0.1:8000/
9. Enter username: univision, pass: testing$47
10. Now you can start your tests

Please contact me via email at andresr56@gmail.com
