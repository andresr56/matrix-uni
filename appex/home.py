from django.http import HttpResponse
from django.template import loader
from django.http import HttpResponse
#import numpy as np
from django.views import View
from django.shortcuts import render
from appex.matrx import Matrx
from time import time


class Home(View):

    def __init__(self):
        self.limit = 0

    template_home = 'matrix/index.html'

    def get(self, request):
        return render(request, self.template_home)

    def post(self, request, *args, **kwargs):
        self.limit = request.POST['limit']
        error = []
        matrix = ''
        cities = []
        roads = {}
        if not self.limit :
            error = ['Please enter a number']
        elif int(self.limit) < 5:
            error = ['Please enter a number > 4']
        else:
            h = self
            m = Matrx(h)
            start_time = time()
            matrix = m.index(int(self.limit))
            elapsed_time = time() - start_time
            print("Elapsed time matrix: %.10f seconds." % elapsed_time)
            start_time = time()
            cities = m.cities(int(self.limit))
            elapsed_time = time() - start_time
            print("Elapsed time cities: %.10f seconds." % elapsed_time)
            start_time = time()
            graph = m.buildgraph(cities, matrix)
            elapsed_time = time() - start_time
            print("Elapsed time graph: %.10f seconds." % elapsed_time)
            start_time = time()
            roads = {}
            for i, init in enumerate(graph):
                list = graph[init]
                for end in list:
                    roads[init + ' to ' + end[0]] = []
                    w, p = m.shortway(graph, init, end[0])
                    roads[init + ' to ' + end[0]] = p, w
            elapsed_time = time() - start_time
            print("Elapsed time roads: %.10f seconds." % elapsed_time)
        return render(request, self.template_home, {'errors': error, 'matrix':  matrix, 'cities': cities, 'roads': roads})
