from django.conf.urls import url
#from appex.views import Viewinit
from appex.accounts import Accounts
from appex.accounts import logout
from appex.views import api_roads
from appex.home import Home
from appex.test_matrix import TestMatrix
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^login/$', Accounts.as_view()),
    url(r'^login/logout/$', auth_views.logout, {'next_page': '/login'}, name='logout'),
    url(r'^$', login_required(Home.as_view(), login_url='matrix/')),
    url(r'^matrix/$', login_required(api_roads, login_url='../login/'), name='api_roads'),
]