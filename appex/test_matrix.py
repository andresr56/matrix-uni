import unittest
import matrx


class TestMatrix(unittest.TestCase):
    m = matrx.Matrx(5)

    def test_buildways(self):
        print ("test build roads")
        self.assertEqual(len(self.m.main()[1]), 20)
        self.assertEqual(len(self.m.main()[1]['C2C3']), 2)

    def test_buildcities(self):
        name = 'C'
        print "test build cities"
        for i, citie in enumerate(self.m.main()[2]):
            i += 1
            value = name + `i`
            self.assertEqual(citie, value)
            self.assertRaises(TypeError, citie, value)

if __name__ == "__main__":
    unittest.main()