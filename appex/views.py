from appex.matrx import Matrx
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['GET', 'POST'])
def api_roads(request):
    error = 'Only method POST'
    if request.method == 'POST':
        limit = request.POST['limit']
        if not limit:
            error = 'Please enter a number'
        elif int(limit) < 5:
            error = 'Please enter a number > 4'
        else:
            m = Matrx(int(limit))
            matrix, roads, cities = m.main()
            return Response({"matrix": matrix, "cities": cities, "roads":  roads})
    return Response({"error": error})
