import numpy as np
from threading import Thread
import heapq


class Matrx:
    def __init__(self, limit):
        self.lim = limit
        self.cities = []
        self.matrix = [[]]
        self.graph = {}
        self.roads = {}
        self.edges = {}

    def main(self):
        high = 200
        low = 50
        self.matrix = np.random.randint(low, high=high, size=(self.lim, self.lim))

        thread = list()
        t = Thread(target=self.buildcities())
        thread.append(t)
        t = Thread(target=self.acrossvalues)
        thread.append(t)
        t.start()

        td = Thread(target=np.fill_diagonal, args=(self.matrix, 0))
        td.start()

        threadtwo = list()
        tg = Thread(self.buildgraphdict())
        threadtwo.append(tg)
        tg = Thread(target=self.buildwayslist())
        threadtwo.append(tg)
        tg.start()
        return self.matrix, self.result, self.cities

    def acrossvalues(self):
        i = 0
        while i < self.lim:
            self.matrix[i:self.lim, i] = self.matrix[i, i:self.lim]
            i += 1

    def buildcities(self):
        name = 'C'
        self.cities = [name + `i` for i in range(1, (self.lim + 1))]

    def buildgraphdict(self):
        for k, mtx in enumerate(self.matrix):
            self.edges[self.cities[k]] = {}
            for j, appex in enumerate(mtx):
                if self.cities[k] != self.cities[j]:
                    self.edges[self.cities[k]].update({self.cities[j]: self.matrix[k][j]})

    def buildwayslist(self):
        self.result = {}
        for edge in self.edges:
            for e in self.edges[edge]:
                self.result[edge+e] = (self.shortest_path(edge, e))

    def shortest_path(self, start, end):
        G = self.edges
        def flatten(L):  # Flatten linked list of form [0,[1,[2,[]]]]
            while len(L) > 0:
                yield L[0]
                L = L[1]

        q = [(0, start, ())]  # Heap of (cost, path_head, path_rest).
        visited = set()  # Visited vertices.
        while True:
            (cost, v1, path) = heapq.heappop(q)
            if v1 not in visited:
                visited.add(v1)
                if v1 == end:
                    c = cost
                    return list(flatten(path))[::-1] + [v1], c
                path = (v1, path)
                for (v2, cost2) in G[v1].iteritems():
                    if v2 not in visited:
                        heapq.heappush(q, (cost + cost2, v2, path))

    # # NO USE
    # def buildgraphlist(self):
    #     for k, mtx in enumerate(self.matrix):
    #         for j, appex in enumerate(mtx):
    #             if self.cities[k] != self.cities[j]:
    #                 tupla = (self.cities[k], self.cities[j], self.matrix[k][j])
    #                 self.edges.append(tupla)

    # NO USE
    # def dijkstra(self, f, t):
    #     g = defaultdict(list)
    #     for l, r, c in self.edges:
    #        g[l].append((c, r))
    #     """When applying merge order (mergesort) looking for execution time in the worst case O (nlgn)
    #      for the distribution of the graph by city in dijkstra were obtained longer times, so the code is commented.
    #      You can check by deleting the comments and Commenting the for cycle up"""
    #     #self.mergeSort(self.edges, g)
    #
    #     q, seen = [(0, f, ())], set()
    #     while q:
    #         (cost, v1, path) = heappop(q)
    #         if v1 not in seen:
    #             seen.add(v1)
    #             path = (v1, path)
    #             if v1 == t: return cost, path
    #
    #             for c, v2 in g.get(v1, ()):
    #                 if v2 not in seen:
    #                     heappush(q, (cost + c, v2, path))
    #
    #     return float("inf")
    #
    # # NO USE
    # def mergeSort(self, alist, g):
    #     if len(alist) > 1:
    #         mid = len(alist) // 2
    #         lefthalf = alist[:mid]
    #         righthalf = alist[mid:]
    #
    #         self.mergeSort(lefthalf, g)
    #         self.mergeSort(righthalf, g)
    #
    #         i = 0
    #         j = 0
    #         k = 0
    #         while i < len(lefthalf) and j < len(righthalf):
    #             if lefthalf[i] < righthalf[j]:
    #                 if (lefthalf[i][2], lefthalf[i][1]) not in g[lefthalf[i][0]]:
    #                     g[lefthalf[i][0]].append((lefthalf[i][2], lefthalf[i][1]))
    #                 i = i + 1
    #             else:
    #                 if (righthalf[j][2], righthalf[j][1]) not in g[righthalf[j][0]]:
    #                     g[righthalf[j][0]].append((righthalf[j][2], righthalf[j][1]))
    #                 j = j + 1
    #             k = k + 1
    #
    #         while i < len(lefthalf):
    #             if (lefthalf[i][2], lefthalf[i][1]) not in g[lefthalf[i][0]]:
    #                 g[lefthalf[i][0]].append((lefthalf[i][2], lefthalf[i][1]))
    #             i = i + 1
    #             k = k + 1
    #
    #         while j < len(righthalf):
    #             if (righthalf[j][2], righthalf[j][1]) not in g[righthalf[j][0]]:
    #                 g[righthalf[j][0]].append((righthalf[j][2], righthalf[j][1]))
    #             j = j + 1
    #             k = k + 1