from django.http import HttpResponse
from django.views import View
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.shortcuts import HttpResponseRedirect

class Accounts(View):
    template_name = 'matrix/login.html'
    #form_class = MyForm

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            #return render(request, 'myapp/login_error.html')
            return render(request, self.template_name)
        else:
            return redirect('../')


    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('../', request.path)
        else:
            return render(request, self.template_name)

    def logout(self, request):
        logout(request)
        return render(request, self.template_name)

