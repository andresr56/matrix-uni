var frm = $('#start_limit');
frm.submit(function (e) {
    let alert = $(".alert-danger");
    alert.hide();
    let start = new Crono();
    let eachJson = new FORMATdata();
    e.preventDefault();
    $("#roadsTable > tbody").html("");
    $("#matrixTable > thead, #matrixTable > tbody").html("");
    if (typeof start.control != 'undefined') start.reinit();
    if (ACTIVEGRAPH){
        document.getElementById('bar').style.width = '0%';
        document.getElementById('text').innerHTML = '0%';
        $("#loadingBar").show();
    }
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        beforeSend: function (request, settings) {
            start_time = new Date().getTime();
            start.init();
            $(".btn-danger").prop("disabled", true);
            disabled = "disabled";
            $("#heading-tbl-matrix").html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading Dashboard of cities...');
            $("#heading-tbl-roads").html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading Road more short...');
            if (!$('#container_matrix').is(":visible")) $("#container_matrix").show();
            if (!$('#container_roads').is(":visible")) $("#container_roads").show();
        },
        success: function (data){
            start.stop();
            $(".btn-danger").prop("disabled", false);
            if (typeof data.error === 'undefined'){
                eachJson.buildData(data);
            } else{
                alert.html(data.error);
                alert.show();
            }
        },
        error: function (data) {
            console.log('An error occurred.');
            console.log(data);
        },
    });
});
