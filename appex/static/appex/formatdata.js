function FORMATdata() {
    this.data = '';
}

FORMATdata.prototype.buildData = function (data) {
    response = data;
    this.buildMatrix(data.matrix, data.cities);
    this.buildRoads(data.roads);
    if (ACTIVEGRAPH) buildGraph(nodes, edges);
}

FORMATdata.prototype.buildMatrix = function (matrices, cities) {
    let tableHead = $('#matrixTable').find('thead');
    let tableBody = $('#matrixTable').find('tbody');
    let tr = $('<tr></tr>').appendTo(tableHead);
    let td = $('<td><span class="glyphicon glyphicon-plane" aria-hidden="true"></span></td>').appendTo(tr);
    nodes = []
    $.each(cities, function (c, citie) {
        td = $('<td>' + citie + '</td>').appendTo(tr);
        nodes[c] = {id: citie, value: 150, label: citie, "group": c}
    });
    $.each(matrices, function (m, matrix) {
        let tr = $('<tr></tr>').appendTo(tableBody);
        $('<td>C' + (m + 1) + '</td>').appendTo(tr);
        $.each(matrix, function (i, mat) {
            $('<td>' + mat + '</td>').appendTo(tr);
        });
    });
    $("#heading-tbl-matrix").html("Dashboard of cities");
}

FORMATdata.prototype.buildRoads = function (roads) {
    edges = [];
    let j = 0;
    $.each(roads, function (i, item) {
        let title = roads[i][0][0] +' to '+roads[i][0][(roads[i][0].length-1)];
        $('#roadsTable').append('<tr><td>' + title + '</td><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
        let color = "#"+((1<<24)*Math.random()|0).toString(16);
        if (roads[i][0].length > 2){
            init = roads[i][0][0];
            for (k = 1; k < (roads[i][0].length); k++){
                edges[j] = {from: init, to: roads[i][0][k], color: color, length: item[1], value: 3*(2+j), title: title};
                init = roads[i][0][k];
                j ++;
            }
        }else{
            edges[j] = {from: roads[i][0][0], to: roads[i][0][(roads[i][0].length-1)], color: color, value: 10, title: title};
            j ++;
        }
    });
    $("#heading-tbl-roads").html("Road more short");
}