#
# #list = [3,56,78,8,9,2,0]
# #print list
#
#
# def quicksort(a, p, r):
#     if p < r:
#         q = partition(a, p, r);
#         #print q
#         quicksort(a, p, q);
#         quicksort(a, q + 1, r);
#     print a
#
# def partition(a, p, r):
#     pivot = a[p];
#     i = p;
#     j = r;
#     while (1):
#         while a[i] < pivot and a[i] != pivot:
#             i += 1
#         while a[j] > pivot and a[j] != pivot:
#             j -= 1
#         if i < j:
#             temp = a[i]
#             a[i] = a[j]
#             a[j] = temp
#         else:
#             return j;
#     #print a
#
# #quicksort(list, 1, 5)
#
# parent = dict()
# rank = dict()
#
#
# def make_set(vertice):
#     parent[vertice] = vertice
#     rank[vertice] = 0
#
#
# def find(vertice):
#     if parent[vertice] != vertice:
#         parent[vertice] = find(parent[vertice])
#     return parent[vertice]
#
#
# def union(vertice1, vertice2):
#     root1 = find(vertice1)
#     root2 = find(vertice2)
#     if root1 != root2:
#         if rank[root1] > rank[root2]:
#             parent[root2] = root1
#         else:
#             parent[root1] = root2
#         if rank[root1] == rank[root2]: rank[root2] += 1
#
#
# def kruskal(graph):
#     for vertice in graph['vertices']:
#         make_set(vertice)
#         minimum_spanning_tree = set()
#         edges = list(graph['edges'])
#         edges.sort()
#     # print edges
#     for edge in edges:
#         weight, vertice1, vertice2 = edge
#         if find(vertice1) != find(vertice2):
#             union(vertice1, vertice2)
#             minimum_spanning_tree.add(edge)
#
#     return sorted(minimum_spanning_tree)

graph = {
        'vertices': ['C1', 'C2', 'C3', 'C4', 'C5'],
        'edges': set([
            (114, 'C1', 'C2'),
            (30, 'C1', 'C3'),
            (195, 'C1', 'C4'),
            (54, 'C1', 'C5'),
            (114, 'C2', 'C1'),
            (145, 'C2', 'C3'),
            (193, 'C2', 'C4'),
            (79, 'C2', 'C5'),
            (30, 'C3', 'C1'),
            (145, 'C3', 'C2'),
            (90, 'C3', 'C4'),
            (89, 'C3', 'C5'),
            (195, 'C4', 'C1'),
            (193, 'C4', 'C2'),
            (90, 'C4', 'C3'),
            (195, 'C4', 'C5'),
            (54, 'C5', 'C1'),
            (79, 'C5', 'C2'),
            (89, 'C5', 'C3'),
            (159, 'C5', 'C4'),
            ])
        }
minimum_spanning_tree = set([
            (1, 'A', 'B'),
            (2, 'B', 'D'),
            (1, 'C', 'D'),
            ])
#print graph['edges']
#assert kruskal(graph) == minimum_spanning_tree
#print kruskal(graph)

from collections import Mapping
from collections import KeysView as VistaDeVertices
from collections import MappingView as _MappingView
from collections import Counter
from itertools import combinations
from random import choice, getrandbits, random


class VistaDeAristas(_MappingView):
    def __len__(self):
        return self._mapping._tamano

    def __contains__(self, arista):
        u, v = arista
        return u in self._mapping and v in self._grafo[u]

    def __iter__(self):
        if self._mapping.dirigido:
            for u in self._mapping:
                for v in self._mapping[u]:
                    yield (u, v)
        else:
            indice = {u:i for i, u in enumerate(self._mapping.vertices)}
            for u in self._mapping:
                for v in self._mapping[u]:
                    if indice[u] < indice[v]:
                        yield (u, v)
### COLAS DE PRIORIDAD MINIMA ###

def _padre(indice): return (indice - 1) // 2


def _hijo_izquierdo(indice): return 2 * indice + 1


def _hijo_derecho(indice): return 2 * (indice + 1)


class ColaMin(Mapping):
    def __init__(self, prioridades=None):
        self._elementos = []  # arreglo de elementos
        self._prioridades = []  # arreglo de prioridades
        self._asas = dict()  # tabla asociativa de elementos a indices
        if prioridades is not None:
            if not isinstance(prioridades, dict):
                raise TypeError('')
            for (elemento, prioridad) in prioridades.items():
                self.introducir_con_prioridad(elemento, prioridad)

    def introducir_con_prioridad(self, elemento, prioridad):
        self._elementos.append(elemento)
        self._prioridades.append(prioridad)
        self._asas[elemento] = len(self) - 1
        self._flotar(len(self) - 1)

    def extraer_min(self):
        if not self:  #Es vacio
            raise IndexError('No se puede extraer de una ColaMin vacia')
        self._intercambiar(0, len(self) - 1)  # Intercambiar primero con ultimo
        # Extraer el ultimo elemento:
        elemento = self._elementos.pop()
        self._prioridades.pop()
        del self._asas[elemento]
        self._hundir(0)  # Recuperar la propiedad del monticulo
        return elemento

    def bajar_prioridad(self, elemento, prioridad):
        i = self._asas[elemento]  # Obtener indice del elemento actual
        if self._prioridades[i] <= prioridad:
            raise ValueError('La prioridad debe ser menor que la actual')
        self._prioridades[i] = prioridad  # Bajar prioridad
        self._flotar(i)  # Recuperar la propiedad del monticulo

    def _intercambiar(self, i, j):
        elem_i, elem_j = self._elementos[i], self._elementos[j]
        prio_i, prio_j = self._prioridades[i], self._prioridades[j]
        self._elementos[i], self._elementos[j] = elem_j, elem_i
        self._asas[elem_i], self._asas[elem_j] = j, i
        self._prioridades[i], self._prioridades[j] = prio_j, prio_i

    def _flotar(self, i):
        while i > 0 and self._prioridades[i] < self._prioridades[_padre(i)]:
            self._intercambiar(i, _padre(i))
            i = _padre(i)

    def _hundir(self, i):
        while True:
            izq, der, hmin = _hijo_izquierdo(i), _hijo_derecho(i), i
            if izq < len(self) and \
                            self._prioridades[izq] < self._prioridades[hmin]:
                hmin = izq
            if der < len(self) and \
                            self._prioridades[der] < self._prioridades[hmin]:
                hmin = der
            if hmin != i:
                self._intercambiar(i, hmin)
                i = hmin
            else:
                break

    def __len__(self):
        return len(self._elementos)

    def __iter__(self):
        for elemento in self._elementos:
            yield elemento

    def __getitem__(self, elemento):
        return self._prioridades[self._asas[elemento]]

    def __repr__(self):
        if not self:
            return 'ColaMin()'  # Monticulo vacio
        t = ['{!r}:{!r}'.format(x, p) for x, p in self.items()]
        return 'ColaMin({{{}}})'.format(', '.join(t))

inf = float('inf')

def relajar(G, u, v, dist, padre):
    d = dist[u] + G[u][v]       #Estimar distancia de s a v pasando por u---v
    if  d < dist[v]:            #Es un camino mas corto
        dist[v], padre[v] = d, u    #Actualizar distancia y camino mas corto
        return True             #Hubo cambios
    return False #No hubo cambios

def inicializar_caminos_mas_cortos(G, s):
    dist, padre = {u: inf for u in G}, {v: v for v in G}
    dist[s] = 0  # distancia de s a si mismo es 0
    return dist, padre


def dijkstra(G, s):
    dist, padre = inicializar_caminos_mas_cortos(G, s)
    H = ColaMin(dist)  # Crear una cola con prioridad minima dist[.]
    while H:  # Todavia hay vertices por explorar
        u = H.extraer_min()  # Elegir el mas cercano a s
        for v in G[u]:  # Actualizar caminos de sus vecinos
            if v in H and relajar(G, u, v, dist, padre):
                H.bajar_prioridad(v, dist[v])
    return dist, padre


class Grafo(dict):
    def __init__(self, V=None, E=None, pesado=False, dirigido=True):
        self._pesado = pesado
        self._dirigido = dirigido
        self._tamano = 0
        if V:
            for u in V:
                self.trazar(u)
        if E:
            if pesado:
                for (u, v) in E:
                    self.trazar(u, E[u, v], v)
            else:
                for (u, v) in E:
                    self.trazar(u, v)

    @property
    def vertices(self):
        return VistaDeVertices(self)

    @property
    def aristas(self):
        return VistaDeAristas(self)

    def vecinos(self, u):
        return VistaDeVertices(self[u])

    def copia(self):
        G = Grafo()
        G.update({u: self[u].copy() for u in self})
        G._pesado = self._pesado
        G._dirigido = self._dirigido
        G._tamano = self._tamano
        return G

    def trazar(self, *camino):
        def agregar_vertice(u):
            if u not in self:
                self[u] = dict() if self.pesado else set()

        def agregar_arista(u, v, w):
            if v not in self[u]: self._tamano += 1
            if self.pesado:
                self[u][v] = w
                if not self.dirigido: self[v][u] = w
            else:
                self[u].add(v)
                if not self.dirigido: self[v].add(u)

        elementos, w = iter(camino), None
        u = next(elementos, None)
        if u is not None: agregar_vertice(u)
        while True:
            if self._pesado: w = next(elementos, None)
            v = next(elementos, None)
            if v is not None:
                agregar_vertice(v)
            else:
                break
            agregar_arista(u, v, w)
            u = v

    def borrar(self, *camino):
        def quitar_arista(u, v):
            if self.pesado:
                if v in self[u]:
                    del self[u][v]
                    if not self.dirigido: del self[v][u]
            else:
                if v in self[u]:
                    self[u].remove[v]
                    if not self.dirigido: self[v].remove[u]

        def quitar_vertice(u):
            if self.pesado:
                for v in self:
                    if u in self[v]:
                        del self[v][u]
            else:
                for v in self:
                    if u in self[v]:
                        self[v].remove(u)
            del self[u]

        if len(camino) == 1:
            quitar_vertice(camino[0])
        elif camino:
            for i in range(len(camino) - 1):
                quitar_arista(camino[i], camino[i + 1])

    @property
    def dirigido(self):
        return self._dirigido

    @property
    def pesado(self):
        return self._pesado

    def voltear(self):
        if not self.dirigido:
            return self.copia()
        H = Grafo(self.vertices, pesado=self.pesado, dirigido=self.dirigido)
        if self.pesado:
            for u in self:
                for v in self[u]:
                    H[v][u] = self[u][v]
        else:
            for u in self:
                for v in self[u]:
                    H[v].add(u)
        H._tamano = self._tamano
        return H

    @staticmethod
    def arbol(pred):
        T = Grafo()
        for u in pred:
            if pred[u] == u:
                T.trazar(u)
            else:
                T.trazar(pred[u], u)
        return T

    @staticmethod
    def aleatorio(n, p, pesos=None, dirigido=False):
        V = [chr(i) for i in range(ord('A'), ord('A') + n)]
        if pesos is not None: pesos = list(pesos)
        G = Grafo(dirigido=dirigido, pesado=bool(pesos))
        for u in V: G.trazar(u)
        for u, v in combinations(V, 2):
            if random() > p: continue
            if dirigido and getrandbits(1): u, v = v, u
            if pesos:
                G.trazar(u, choice(pesos), v)
            else:
                G.trazar(u, v)
        return G

    def __repr__(self):
        if self:
            args = [repr(set(self.vertices)), repr(set(self.aristas))]
        else:
            args = []
        if self.pesado:
            args.append('pesado = True')
        if not self.dirigido:
            args.append('dirigido = False')
        return 'Grafo({})'.format(', '.join(args))

    def __str__(self):
        def factorizar(u):
            c = [u]
            while G[u]:
                v = G[u].pop()
                if not G.dirigido: G[v].remove(u)
                c.append(v)
                u = v
            return c

        def extender(c):
            i = 0
            while True:
                while i < len(c) and not G[c[i]]:
                    i += 1
                if i == len(c): return c
                c[i:i + 1] = factorizar(c[i])

        def descomponer():
            if G.dirigido:
                H = G.voltear()
                L = [u for u in G if not (G[u] or H[u])]
                Q = list(Counter({u: len(G[u]) - len(H[u]) for u in G}) \
                         .elements())
            else:
                L = [u for u in G if not G[u]]
                Q = {u for u in G if len(G[u]) % 2}

            while Q:
                L.append(factorizar(Q.pop()))
                if not G.dirigido: Q.remove(L[-1][-1])
            for c in L: extender(c)
            while True:
                u = next((u for u in G if G[u]), None)
                if u is None: break
                L.append(extender(factorizar(u)))
            return L

        def aristas(camino):
            for i in range(len(camino) - 1):
                yield camino[i], camino[i + 1]

        G = Grafo(self.vertices, self.aristas, dirigido=self.dirigido)
        caminos = descomponer()
        lineas = []
        cabeza = '>' if self.dirigido else '-'
        if self.pesado:
            for camino in caminos:
                abajo = [str(camino[0])]
                arriba = [' ' * len(abajo[0])]
                for (u, v) in aristas(camino):
                    arriba.append(' ' + str(self[u][v]))
                    abajo.append('-' * len(arriba[-1]))
                    abajo.append(cabeza + str(v))
                    arriba.append(' ' * len(abajo[-1]))
                lineas.append(''.join(arriba))
                lineas.append(''.join(abajo))
        else:
            arista = '-' + cabeza
            for camino in caminos:
                lineas.append(arista.join(camino))
        return '\n'.join(lineas)

G = Grafo(pesado = True)
G.trazar('C1', 2, 'C2', 3, 'C3', 4, 'C4', 7, 'C5')
G.trazar('C1', 4, 'C4', 1, 'C3', 6, 'C2', 7, 'C5')
#G.trazar('C1', 14, 'C4')#, 5, 'C2', 7, 'C4', 8, 'C5')
G.trazar('C1', 15, 'C5')#, 6, 'C2', 7, 'C3', 9, 'C5')
#G.trazar('C1', 0, 'C1')#, 7, 'C2', 8, 'C3', 9, 'C4')
print(G)
print()

dist, padre = dijkstra(G, 'C1')
print('arbol generado por el algoritmo de Dijkstra iniciando en A:')
print(Grafo.arbol(padre))
print()
print('Distancias encontradas por el algoritmo:')
for u in dist:
    print('d({}, {}) = {}'.format('C1', u, dist[u]))
print()