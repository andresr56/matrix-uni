from django.http import HttpResponse
from django.template import loader
from django.http import HttpResponse
import numpy as np
from django.views import View
from django.shortcuts import render
from matrix.matrx import Matrx


class Home(View):

    def __init__(self):
        self.limit = 0

    template_home = 'matrix/index.html'

    def get(self, request):
        return render(request, self.template_home)

    def post(self, request, *args, **kwargs):
        self.limit = request.POST['limit']
        error = []
        matrix = ''
        if not self.limit :
            error = ['Please enter a number']
        elif int(self.limit) < 2:
            error = ['Please enter a number > 1']
        else:
            h = self
            m = Matrx(h)
            matrix = m.index(int(self.limit))
            cities = m.cities(int(self.limit))
            graph = m.buildgraph(cities, matrix)
            roads = {}
            for i, init in enumerate(graph):
                list = graph[init]
                for end in list:
                    roads[init + ' to ' + end[0]] = []
                    w, p = m.shortway(graph, init, end[0])
                    roads[init + ' to ' + end[0]] = p, w
        return render(request, self.template_home, {'errors': error, 'matrix':  matrix, 'cities': cities, 'roads': roads})
